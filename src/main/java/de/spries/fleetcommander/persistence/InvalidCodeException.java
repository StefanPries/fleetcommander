package de.spries.fleetcommander.persistence;

public class InvalidCodeException extends Exception {

	public InvalidCodeException(String msg) {
		super(msg);
	}

}
