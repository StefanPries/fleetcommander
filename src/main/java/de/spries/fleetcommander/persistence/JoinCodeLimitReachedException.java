package de.spries.fleetcommander.persistence;

public class JoinCodeLimitReachedException extends Exception {

	public JoinCodeLimitReachedException(String msg) {
		super(msg);
	}

}
