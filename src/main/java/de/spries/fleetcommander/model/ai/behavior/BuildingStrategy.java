package de.spries.fleetcommander.model.ai.behavior;

import de.spries.fleetcommander.model.facade.PlayerSpecificUniverse;

public interface BuildingStrategy {

	void buildFactories(PlayerSpecificUniverse universe);

}
