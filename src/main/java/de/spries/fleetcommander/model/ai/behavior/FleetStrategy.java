package de.spries.fleetcommander.model.ai.behavior;

import de.spries.fleetcommander.model.facade.PlayerSpecificUniverse;

public interface FleetStrategy {

	void sendShips(PlayerSpecificUniverse universe);

}
