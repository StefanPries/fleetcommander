package de.spries.fleetcommander.model.core.common;

public class IllegalActionException extends RuntimeException {

	public IllegalActionException(String msg) {
		super(msg);
	}
}
